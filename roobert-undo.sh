sensuctl asset delete sensu-ruby-runtime;

sensuctl asset delete sensu-plugins-cpu-checks;

sensuctl asset delete sensu-plugins-memory-checks;

sensuctl asset delete sensu-plugins-load-checks;
  
sensuctl check delete check-cpu;

sensuctl check delete check-memory;

sensuctl check delete check-swap;

sensuctl check delete check-load;
