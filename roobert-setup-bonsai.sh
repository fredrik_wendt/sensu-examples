sensuctl asset add sensu/sensu-ruby-runtime --rename sensu-ruby-runtime



sensuctl asset add sensu-plugins/sensu-plugins-cpu-checks --rename sensu-plugins-cpu-checks



sensuctl asset add sensu-plugins/sensu-plugins-memory-checks --rename sensu-plugins-memory-checks



sensuctl asset add sensu-plugins/sensu-plugins-load-checks --rename sensu-plugins-load-checks


  
sensuctl check create check-cpu \
  --command 'check-cpu.rb -w 90 -c 97' \
  --interval 60 \
  --subscriptions dom0 \
  --runtime-assets sensu-plugins-cpu-checks,sensu-ruby-runtime

sensuctl check create check-memory \
  --command 'check-memory-percent.rb -w 90 -c 97' \
  --interval 60 \
  --subscriptions dom0 \
  --runtime-assets sensu-plugins-memory-checks,sensu-ruby-runtime

sensuctl check create check-swap \
  --command 'check-swap-percent.rb -w 90 -c 97' \
  --interval 60 \
  --subscriptions dom0 \
  --runtime-assets sensu-plugins-memory-checks,sensu-ruby-runtime

sensuctl check create check-load \
  --command 'check-load.rb -w 5,3,2 -c 10,5,3' \
  --interval 60 \
  --subscriptions dom0 \
  --runtime-assets sensu-plugins-load-checks,sensu-ruby-runtime
